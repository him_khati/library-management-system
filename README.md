# Library Management System

## Repo Intro ##

* Library Management system is a desktop application used to maintain database of the Library.
* Version 1.0


## How do I get set up?

### Software requirement ###

* MySql and JRE


## Setup Instruction ##

* set mySql password to null from root user
* create database using commands give in "Database commands LMS"
* Ener some sample data
* execute run executable Version/Library_Management.jar

## Screenshots ##

https://bitbucket.org/him_khati/library-management-system/src/e8bb5cae3487fed9be2ef81580fa3d3248988bc5/Snapshots/?at=master